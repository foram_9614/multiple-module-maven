package org.product.controller;

import java.util.List;

import org.product.entity.Product;
import org.product.exception.ProductException;
import org.product.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@Transactional(readOnly = true)
public class ProductController {

	@Autowired
	ProductServiceImpl productService;

	@RequestMapping("/")
	public String home(Model model) {
		List<Product> products = productService.getAll();
		model.addAttribute("productList", products);
		return "home";
	}

	@RequestMapping(value = "/addProductForm")
	public String showAddForm(ModelMap map) {
		Product product = new Product();
		map.addAttribute("product", product);
		return "addForm";
	}

	@RequestMapping(value = "/addProduct")
	public String addProduct(Product product, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "addForm";
		}

		try {
			productService.addProduct(product);
			List<Product> products = productService.getAll();
			model.addAttribute("productList", products);
			return "home";
		} catch (ProductException e) {
			// TODO Auto-generated catch block
			model.addAttribute("exception", e);
			return "error";
		}

		
	}

	// @RequestMapping("/editIdForm")
	// public String showIdForm(){
	// return "editIdForm";
	// }
	//
	// @RequestMapping("/editProductForm")
	// public String showEditForm(@RequestParam("productId") Integer productId,
	// Model model){
	// Product product = productService.getProductByById(productId);
	// if(product==null){
	// return "editIdForm";
	// }
	// model.addAttribute("product", product);
	// return "editForm";
	// }
	//
	// @RequestMapping("/editProduct")
	// public String editProduct(@ModelAttribute("product") Product product,
	// BindingResult result){
	// if(result.hasErrors()){
	// return "editForm";
	// }
	// productService.editProduct(product);
	// return "admin";
	// }
	//
	// @RequestMapping(value="/searchProducts")
	// public String getProductsByCity(@RequestParam("search")String category,
	// Model model){
	// List<Product> products = productService.getProductsByCity(category);
	// if(products.size()==0){
	// return "home";
	// }model.addAttribute("productsList", products);
	// return "home";
	// }
	//

}
