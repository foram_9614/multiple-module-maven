<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h4>Add hotel</h4>
	<form:form action="editProduct" commandName="product">
		<table>

			<tr>
				<td>productID</td>
				<td><form:input path="productID" /></td>
				<td><form:errors path="productID" /></td>
			</tr>

			<tr>
				<td>productName</td>
				<td><form:input path="productName" /></td>
				<td><form:errors path="productName" /></td>
			</tr>

			<tr>
				<td>productPrice</td>
				<td><form:input path="productPrice" /></td>
				<td><form:errors path="productPrice" /></td>
			</tr>

			<tr>
				<td>productQuantity</td>
				<td><form:input path="productQuantity" /></td>
				<td><form:errors path="productQuantity" /></td>
			</tr>

			<tr>
				<td>productCategory</td>
				<td><form:select path="productCategory">
						<form:option value="select">---select---</form:option>
						<form:option value="General">General</form:option>
						<form:option value="Electronic">Electronic</form:option>
					</form:select></td>
			</tr>
		
			<tr>
				<td><input type="submit" value="Edit Hotel"></td>
			</tr>
		</table>

	</form:form>
</body>
</html>