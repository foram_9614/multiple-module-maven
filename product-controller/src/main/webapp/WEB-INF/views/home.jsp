<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<html>
<head>
<title>Home</title>
</head>
<body>

	<a href="addProductForm">Add Product</a><br>

	<!-- <form action="searchProducts">
		<table>
			<tr>
				<td>Search:</td>
				<td><input type="text" name="search"></td>
				<td><input type="submit" name="Go"></td>

			</tr>
		</table>
	</form> -->

	<br>
	<br>

	<table>
		<tr>
			<th>productID</th>
			<th>productName</th>
			<th>productPrice</th>
			<th>productQuantity</th>
			<th>productCategory</th>
		</tr>
		
		<c:forEach var="product" items="${productList }">
		<tr>
			<td>${product.productID }</td>
			<td>${product.productName }</td>
			<td>${product.productPrice }</td>
			<td>${product.productQuantity }</td>
			<td>${product.productCategory}</td>
			</tr>
		</c:forEach>
	</table>
</body>
</html>
