package org.product.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.product.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDao extends JpaRepository<Product, Integer>{

	List<Product> findByProductCategory(String category);

//	private final SessionFactory sessionFactory;
//
//	@Autowired
//	public ProductDao(SessionFactory sessionFactory) {
//		this.sessionFactory = sessionFactory;
//	}
//
//	public Product findOne(int productID) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.openSession();
//		Transaction tx = null;
//		Product product = null;
//		try {
//			tx = session.beginTransaction();
//			product = (Product) session.get(Product.class, productID);
//			tx.commit();
//		} catch (HibernateException e) {
//			if (tx != null)
//				tx.rollback();
//			e.printStackTrace();
//		}
//
//		return product;
//	}
//
//	public List<Product> findAll() {
//		// TODO Auto-generated method stub
//
//		Session session = this.sessionFactory.openSession();
//		List<Product> products = null;
//		Transaction tx = null;
//		try {
//			tx = session.beginTransaction();
//			Query query = session.createQuery("from Product");
//			products = query.list();
//			tx.commit();
//		} catch (HibernateException e) {
//			if (tx != null)
//				tx.rollback();
//			e.printStackTrace();
//		} 
//		return products;
//	}
//
//	public void save(Product product) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.openSession();
//		Transaction tx = null;
//		try {
//			tx = session.beginTransaction();
//			session.save(product);
//			tx.commit();
//		} catch (HibernateException e) {
//			if (tx != null)
//				tx.rollback();
//			e.printStackTrace();
//		} 
//
//	}
//
//	public List<Product> findByProductCategory(String productName) {
//		// TODO Auto-generated method stub
//		Session session = this.sessionFactory.openSession();
//		List<Product> products = null;
//		Transaction tx = null;
//		try {
//			tx = session.beginTransaction();
//			Query query = session.createQuery("from Product where productCategory = ?");
//			query.setParameter(0, productName);
//			products = query.list();
//			tx.commit();
//		} catch (HibernateException e) {
//			if (tx != null)
//				tx.rollback();
//			e.printStackTrace();
//		}
//		return products;
//	}

}
