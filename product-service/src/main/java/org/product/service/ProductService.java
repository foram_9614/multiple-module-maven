package org.product.service;

import java.util.List;

import org.product.entity.Product;
import org.product.exception.ProductException;

public interface ProductService {

	List<Product> getAll();

	Product addProduct(Product product) throws ProductException;
}