package org.product.service;

import org.springframework.stereotype.Service;

import java.util.List;

import org.product.dao.ProductDao;
import org.product.entity.Product;
import org.product.exception.ProductException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	ProductDao productDao;

	@Override
	public List<Product> getAll() {
		// TODO Auto-generated method stub
		return productDao.findAll();
	}

	@Override
	public Product addProduct(Product product) throws ProductException {
		// TODO Auto-generated method stub
		Product product2= null;
		if(product.getProductName()!=""){
			 product2 = productDao.save(product);
		}else{
			throw new ProductException("Name cant be null");
		}
			
	 
		return product2;
	}

	// public Product getProductByById(Integer productId) {
	// // TODO Auto-generated method stub
	// return productDao.findOne(productId);
	// }
	//
	// public void editProduct(Product product) {
	// // TODO Auto-generated method stub
	// productDao.save(product);
	// }
	//
	// public List<Product> getProductsByCity(String category) {
	// // TODO Auto-generated method stub
	// return productDao.findByProductCategory(category);
	// }

}
